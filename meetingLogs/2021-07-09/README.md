This Week
=============
Checked MadGraph's `scan` functionality and compared it with Contur's `param_file.dat`.  
  
*NOTES*  
1. MadGraph can also scan variables using `scan`. It also works with python's math library.
e.g. Both syntaxes give you same array of physics parameters to scan.
```
set param_card X scan:[100,200,300,400]
set param_card X scan:[i*100 for i in range(1,5)]
set param_card X scan:[i*math.log(10000) for i in range(1,5)]
```
  
2. MadGraph and Contur has a slight different structure to store the results.
MadGraph : `{WorkingDir}/Events/run_X/`
Contur : `{WorkingDir=myscan}/13TeV/X/`
[Comment] Parse the center of mass energy from MadGraph output logs instead of changing the structure of either MadGraph or Contur.  
  
3. Contur needs REL parameters set in the `param_file.dat` to draw the heatmaps with dependent variables.  
e.g. If some BSM model takes X as a scanable BSM physics parameter, to draw the heatmap with `X^{2}` or `log(X)` it needs to be predefined in the `param_file.dat` for the parameter to be written in the grid parameter txt file.
```
# param_file.dat
[[squareX]] # name of the variable
mode=REL # relative mode (dependency on other variables)
form={X}*{X} # define how this variable is dependent to variable X

# single grid parameter txt file
X = 0.01
Y = 10.0
squareX = 0.0001 # dependent parameter is also written although it's not a scannable parameter within the UFO model
```
  
  
Remainining
=============
**To Do - fast**
1. MadGraph setup in UCL server.  
`make_opts` issue seems to be fixed by Olivier. `https://gitlab.com/hepcedar/contur/-/issues/153`  
I have to check whether it works now.
  
2. `Failed to reach target` still seems to be an issue.  
We thought the width being too big for certain parameter sets seem to be the reason. I checked the old logs but there seems to be problem with `mN1=316.23GeV, |VmuN1|=0.01` which has small width. The actual workding directories are cleaned up due to storage reasons.
Rerun the Contur scans and give specific grid points to Olivier.
  
3. Start working on MadGraph bzr developments.  
Olivier already worked on Rivet and Contur installation (Contur installation has some issues due to dependencies on python libraries).  
