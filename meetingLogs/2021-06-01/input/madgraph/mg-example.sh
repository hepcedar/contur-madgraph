set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set loop_optimized_output True
set complex_mass_scheme False
set automatic_html_opening False
set run_mode 0
set nb_core 1
import model /unix/cedar/shjeon/contur/runHeavyN/RunInfo/SM_HeavyN_NLO
define p = u d c s b u~ d~ c~ s~ b~ g
define j = u d c s b u~ d~ c~ s~ b~ g
define ff = mu+ mu- e+ e- ta+ ta- vm vm~ ve ve~ vt vt~ j
define mm = mu+ mu- vm vm~
define vv = z w+ w-
generate p p > mm n1, n1 > mm ff ff
output mgevents
launch
shower=Pythia8
set MN1        {mN1}
set mn2 999999
set mn3 999999
set VeN1 0
set VeN2 0
set VeN3 0
set VmuN1      {VmuN1}
set VmuN2 0
set VmuN3 0
set VtaN1 0
set VtaN2 0
set VtaN3 0
set no_parton_cut
set use_syst False
