**Issues**
- Current workflow can only use HepMC file formats and not FIFO. But Pythia doesn't support paralleization so when one uses FIFO, it would have to use a single core which won't be very helpful when one runs multiple parameter points for scan.  
- Contur installation to be done by Olivier. Once it's done I should work on linking Rivet outputs to Contur in MadGraph.  
- Using existing MadGraph functionalities to parse the rivet cards.
- UFO Model files have some sort of dictionary like file `ident_card.dat`. We can use this to make Contur maps from MadGraph written `param_card.dat`.
