**This Week**

1. Rivet install inside MadGraph works with `https://code.launchpad.net/~maddevelopers/mg5amcnlo/rivet_install`
  
2. Successfully passed the HepMC files from Pythia to Rivet
	- Need to work on a code that reads the `rivet_card.dat` and passes the list of Rivet analyses that user wants to run on
	- Running Rivet with hepmc.gz and hepmc files have not much difference
  
3. There are two options to make Rivet work
	- Using one line executable shell commands
	- Using Rivet's python modules (analysishandler)
	[Comment] Now it is fine to stick with the one above
  
4. There is a bug in `Rivet:ATLAS_2016_I1469071`
	- It is safe to ignore this as this is not really well written to be used for Contur (issues with identifying neutrinos with PdgID)
  
5. Contur split jobs to the working nodes. But MadGraph `scan` runs the job in series so it takes much longer for the jobs to finish
	[Comment] Olivier might work on this, running the next MadGraph jobs when Rivet starts running
	[Comment] Running Rivet with multiple cores is not so easy
  
**To Do**
1. Make a parser code for `rivet_card.dat`. Making it work with `set` commands can be done in a longer term.
2. Make Contur work after the Rivet jobs are finished.
