**This Week**
1. Contur trial runs using Contur-Herwig interface using UCL servers.
2. Used SM_HeavyN_NLO model file.

**To Do - fast**
1. Check the existing Contur-MadGraph interface and see if I can extract any of the codes/scripts that are already written codes.
2. Check whether UCLouvain servers are able to run Contur + Rivet
3. Recheck whether UCL servers are able to run MadGraph

**To Do - slow**
1. Check MadGraph scan functionality : MadGraph also has this scanning functionality just like injecting the parameters with param_file.dat in Herwig. Never used this myself so would be worth checking this.
2. SM_HeavyN_NLO model file's Higgs BR seems to be incorrect : Possibly due to b mass settings (5f scheme in default), but make sure with Richard Ruiz.
3. Update the HeavyN page : https://hepcedar.gitlab.io/contur-webpage/results/HeavyN/index.html
	- N can do 3 body decays with Herwig : Test mN < 80 GeV where secondary W becomes offshell.
	- Can also consider updating the page with MadGraph results in a long term : Can even access the VBF signal samples with MadGraph where it is more sensitive when mN > 500 GeV.
	- Make a plot which scale's are comparable to direct limits from LHC.
